const mongoose = require('mongoose');
const UsersSchema = mongoose.Schema({
    email:String,
    password:String,
    first_name:String,
    last_name:String,
    status:String
})

module.exports = mongoose.model('user',UsersSchema,'users')