const Users = require('../models/users.model.js');
const jwt = require('jsonwebtoken')
const tokenConfig = require('../../config/token.config');
const hash = require('object-hash');

exports.delete = (req, res) => {
    Users.findByIdAndRemove(req.params._id).then(
        user => {
            if(!user){
                return res.status(404).send({
                    message:"Error user not found :",
                })
            }
            res.send({message:"user delete"})
        }
    ).catch(error =>{
        res.status(500).send({
            message: error.message || "Error while retriving data"
        })
    })
}
exports.create = (req, res) => {
    //validate request
    if(!req.body.email){
        return res.status(400).send({
            message:"Email content cannot be empty"
        })
    }
    const passwordHash = hash(req.body.password)
    const user = new Users({
        email:req.body.email,
        password:passwordHash,
        first_name:req.body.first_name || "",
        last_name:req.body.last_name || "",
        status:req.body.status || "",
    })
    user.save().then(data => {
        console.log(data)
        let payload ={subject:data._id}
        let token = jwt.sign(payload,tokenConfig.secret)
        res.send({token})
    }).catch(err => {
        console.log(err)
        res.status(500).send({err,
            message: err.message || "Some error occured while creating the User"
        });
    });   
};

exports.login = (req,res) => {
    Users.findOne({ email: req.body.email}).then( user => {
        if(!user){
            return res.status(404).send({
                message:"Error user not found : "+req.body.email+" User : "+user,
                valideUser: false
            })
        }else {
            //compare les Hashs
            /*
            */
           const passwordHash = hash(req.body.password)
            if (user.password === passwordHash){
                console.log("compare user.password: ", user.password," to req.body.password: ", passwordHash)
                //generate token
                let payload ={subject:user._id}
                let token = jwt.sign(payload,tokenConfig.secret)
                res.send({token})
            }
            else{
                return res.status(404).send({
                    message:"Invalid password",
                    valideUser: false
                })
            }
        }
    })
}

// Retrieve and return all notes from the database.
exports.getAllUsers = async (req,res) => {
    await Users.find().then(users => {
        console.log('returning all users')
        res.send(users)
    }).catch(err => {
        console.log('Error while fetchng all users',err)
        res.status(500).send({
            message: err.message || "Error while retriving data"
        })
    })
}
// Find by Id
exports.getUserById = async (req,res) => {
    await Users.findById(req.params._id).then(user => {
        if(!user){
            console.log('getUserById : User does not exist')
            return res.status(404).send({
                message:"Users not found" + req.params._id
            })
        }
        console.log('getUserById : User found')
        res.send(user)
    }).catch(err => {
        if(err.kind ==="ObjectId"){
            console.log('getUserById : UserId not found')
            return res.status(404).send({
                message: "User not found with id " + req.params._id
            });                
        }
        console.log('getUserById : Error retrieving user')
        return res.status(500).send({
            message: "Error retrieving user with id " + req.params._id
        });
    });
};

//Update a user info
exports.update = (req,res) => {
    Users.findByIdAndUpdate(req.params._id, {
        email:req.body.email,
        password:req.body.password,
        first_name:req.body.first_name || "",
        last_name:req.body.last_name || "",
        status:req.body.status || "Not definied",
    }, {new: true}).then(user => {
        if(!user){
            return res.send({
                message:"User not found "+req.params._id
            })
        }
        res.send(user)
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.noteId
        });
    })
}