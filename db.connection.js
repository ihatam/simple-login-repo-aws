const mongoose = require('mongoose');
const dbConfig = require('./config/database.config.js');
var errHandler = function(err){
    console.log(err)
}
module.exports.connectToAtlasDb = async ()=>{
    const db_url = dbConfig.url
    if(db_url === 'null'){
        await dbConfig.dbUrl.then(JSON.parse(result, (data)=>{
            connect(data.DB_URL)
        }),errHandler)
    }else{
        connect(db_url)
    }
}
module.exports.disconnect = async () =>{
    mongoose.disconnect()
    console.log("Succesfully disconnect to dataabse")
}
function connect(url){
    mongoose.Promise = global.Promise;
    mongoose.connect(url,{
        useNewUrlParser: true
    }).then(() => {
        console.log("Succesfully connect to dataabse")
    }).catch(err => {
        console.log("Fail to connect to database url was:",url," error : ",err)
    })
}
